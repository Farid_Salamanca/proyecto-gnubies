#include <LiquidCrystal.h>
LiquidCrystal lcd(5,4,3,2,1,0);//El orden va del pin mayor al menor.
String game[5] = {"Stone", "Papel", "scissors", "lizard", "Spock"};
int move_U1 = 0;
int move_U2 = 0;
int move_C = 0;
byte option = 0;

void setup(){
  lcd.begin(16, 2);
  for (int i = 6; i <= 18; i++){
    pinMode(i, INPUT);
    }
  randomSeed(millis());
  }

void loop(){
  option = 0;
  lcd.setCursor(0, 0);
  lcd.print("   SELECT THE   ");
  lcd.setCursor(0, 1);
  lcd.print("   GAME  MODE   ");
  delay(1500);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("ARDUINO Vs TU ");
  lcd.setCursor(0, 1);
  lcd.print("PLAYER 1 Vs PLAYER 2");
      
  while(option==0){
    if(digitalRead(13)== HIGH){
    gameMode(1);  
    option = 1;
  }
  else if(digitalRead(12)== HIGH){
    gameMode(2);
    option = 2; 
  }
  }
  

  }
void gameMode(int Parametro){
  if(Parametro == 1){
    delay(500);
    playAgainstArduino();
  }else if(Parametro ==2){
    delay(500);
    playAgainstPlayer();
  }
}

void playAgainstPlayer(){
lcd.clear();
 while (move_U1 == 0){
    lcd.setCursor(0, 0);
    lcd.print("Player 1");
    lcd.setCursor(0, 1);
    lcd.print("Your Turn");
    // Play the user 1
    for (int i = 7; i < 12; i++){
      game_U1(i);
      }
 }
  delay(500); 
  lcd.clear();
   while (move_U2 == 0){
    lcd.setCursor(0, 0);
    lcd.print("Your turn");
    lcd.setCursor(0, 1);
    lcd.print("Player 2");
    // Play the user 2
    for (int i = 14; i < 19; i++){
      game_U2(i);
      }
    }
  // Refresh the LCD with the play from user
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("PL1:");
  lcd_play(move_U1);
  // Jugada de Arduino
  lcd.setCursor(0, 1);
  lcd.print("PL2:");
  lcd_play(move_U2);
  // Jugada de Arduino

  delay(3000);
  // Refresh LCD with the Winner
  selectWinnerMode2(move_U1, move_U2);
  delay(5000);
  move_U1 = 0;
  move_U2 = 0;
}


void playAgainstArduino(){  
lcd.clear();
 while (move_U1 == 0){
    lcd.setCursor(0, 0);
    lcd.print("You go");  
    // Play the user
    for (int i = 7; i <= 11; i++){
      game_U1(i);
      }
    }
  // Update LCD with user play
  lcd.clear();
  lcd.setCursor(6, 0);
  lcd.print("TU:");
  lcd_play(move_U1);
  // Arduino Mode
  if (move_U1 != 0){
    // Play the Arduino
    game_C();
    // Update LCD with Arduino Play
    lcd.setCursor(0, 2);
    lcd.print("PC:");
    lcd_play(move_C);
    }
  delay(3000);
  // Update LCD with the winner
  selectWinnerMode1(move_U1, move_C);
  delay(5000);
  move_U1 = 0;
  move_C = 0;
}
// User 1 move
int game_U1(int pushButton){
  if (digitalRead(pushButton) == HIGH){
    move_U1 = pushButton;
    }
   return move_U1;
  }
// User 2 move
int game_U2(int pushButton){
  if (digitalRead(pushButton) == HIGH){
    move_U2 = pushButton;
    }
   return move_U2;
  }


// Plays of Arduino
int game_C(){
  move_C = random(7, 12);
  return move_C;
  }

// Refresh the  LCD with the plays
void lcd_play(int hand){
    switch (hand){
    case 7:
    case 14:
      lcd.print(game[0]);
      break;
    case 8:
    case 15:
      lcd.print(game[1]);
      break;
    case 9:
    case 16:
      lcd.print(game[2]);
      break;
    case 10:
    case 17:
      lcd.print(game[3]);
      break;
    case 11:
    case 18:
      lcd.print(game[4]);
      break;
    }
  }

// Determina el ganador
void selectWinnerMode1(int user1, int comp){
  if ( //
    user1 == 7 && comp == 9 || user1 == 7 && comp == 10 || //
    user1 == 8 && comp == 7 || user1 == 8 && comp == 11 || //
    user1 == 9 && comp == 8 || user1 == 9 && comp == 10 || //
    user1 == 10 && comp == 8 || user1 == 10 && comp == 11 || //
    user1 == 11 && comp == 7 || user1  == 11 && comp == 9
  ){
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Lástima!");
    lcd.setCursor(0, 1);
    lcd.print("Ganaste :(");
    }
    else if (user1 == comp){
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Hay");
      lcd.setCursor(0, 1);
      lcd.print("    Empate!!    ");
      }
  
    else{
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Arduino");
      lcd.setCursor(0, 1);
      lcd.print("    Won");
      }
  option = 1;
  }
  void selectWinnerMode2(int user1, int user2){
   int user_2 = user2-7;
  if ( //
    user1 == 7 && user_2 == 9 || user1 == 7 && user_2 == 10 || //
    user1 == 8 && user_2 == 7 || user1 == 8 && user_2 == 11 || //
    user1 == 9 && user_2 == 8 || user1 == 9 && user_2 == 10 || //
    user1 == 10 && user_2 == 8 || user1 == 10 && user_2 == 11 || //
    user1 == 11 && user_2 == 7 || user1  == 11 && user_2 == 9
  ){
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("* Player 1 *");
    lcd.setCursor(0, 1);
    lcd.print("Won");
    }
    else if (user1 != user_2){
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("* Player 2 *");
      lcd.setCursor(0, 1);
      lcd.print("Won");
      }
    else{
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("  There  ");
      lcd.setCursor(0, 1);
      lcd.print(" is a tie!! ");
      }
  option = 1;
  }